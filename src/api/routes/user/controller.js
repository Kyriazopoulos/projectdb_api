const userService = require('~services/user');
const authService = require('~services/authentication');

module.exports.login = async (req, res, next) => {
	try {
		const { username, password } = req.body;
		const user = await userService.getByUsername(username);

		if (user.password !== password) throw new Error('Invalid password');

		const type = await userService.getTypeByUsername(username);

		const token = authService.sign(user.username, type);
		res.status(200).json({ ...user, type, token });
	} catch (error) {
		next(error);
	}
};
