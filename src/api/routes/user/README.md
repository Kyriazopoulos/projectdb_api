# Routes

## POST @ /user/login

Expects

```Javascript
    {
        "username": String,
        "password": String
    }
```

Returns

```Javascript
    {
        "username": String,
        "password": String,
        "name": String,
        "reg_date": Date,
        "email": String,
        "type": "admin" | "recruiter" | "candidate",
        "token": String
    }
```
