const router = require('express').Router();
const controller = require('./controller');

router.get('/all', async (req, res, next) => {
	try {
		const result = await dbService.executeQuery('SELECT * FROM user;');

		res.status(200).json(result);
	} catch (error) {
		next(error);
	}
});

router.post('/login', controller.login);

module.exports = router;
