# ACTIONS

## Get history tables:

```
GET @ /admin/history?username={username}&table={table}
```

## Add recruiter:

```
POST @ /admin/recruiter
```

## Add candidate:

```
POST @ /admin/candidate
```

## Add field:

```
POST @ /admin/antikeim
```

## Add firm to field

```
POST @ /admin/field
```

# ROUTES

## GET @ /admin/history?username={username}&table={table}

returns

```Javascript
    [
        ...,
        {
            "hid": Number,
            "username": String,
            "on_date": Date,
            "success": 0 | 1,
            "action": "insert" | "delete" | "update",
            "on_table": "user" | "etaireia" | "recruiter" | "candidate" | "job",
        },
    ]
```

## POST @ /admin/recruiter

expects

```Javascript
    {
        "username": String,
        "password": String,
        "firstname": String,
        "lastname": String,
        "email": String,
        "experience": Number,
        "firm": String
    }
```

returns

```Javascript
    {
        "username": String,
        "password": String,
        "firstname": String,
        "lastname": String,
        "email": String,
        "experience": Number,
        "firm": String
    }
```

## POST @ /admin/candidate

expects

```Javascript
    {
        "username": String,
        "password": String,
        "firstname": String,
        "lastname": String,
        "email": String,
        "bio": String,
        "recomendation": String,
        "certificates": String,
    }

    // Optional
    {
        "languages": "EN, FR, SP, GR", // Example
        "degree": String,
        "graduation": Number, // Graduation year
        "grade": Number, // Graduation grade (ex. 8.0)
        "projects": [
            ...,
            {
                "description": String,
                "url": String
            }
        ]
    }
```

returns

```Javascript
    {
        "username": String,
        "password": String,
        "firstname": String,
        "lastname": String,
        "email": String,
        "bio": String,
        "recomendation": String,
        "certificates": String,
    }

    // Optional
    {
        "languages": "EN, FR, SP, GR", // Example
        "degree": String,
        "projects": [
            ...,
            {
                "description": String,
                "url": String
            }
        ]
    }
```

## POST @ /admin/antikeim

expects

```Javascript
    {
        "title": String,
        "description": String
    }

    // Optional
    {
        "belongs": String // parent antikeim title
    }
```

returns

```Javascript
    {
        "title": String,
        "description": String
    }

    // Optional
    {
        "belongs": String // parent antikeim title
    }
```

## POST @ /admin/field

expects

```Javascript
    {
        "firmName": String,
        "antikeim": String // antikeim title
    }
```

returns

```Javascript
     {
        "firmName": String,
        "antikeim": String // antikeim title
    }
```
