const userService = require('~services/user');
const candidateService = require('~services/candidate');
const languageService = require('~services/language');
const degreeService = require('~services/degree');
const recruiterService = require('~services/recruiter');
const historyService = require('~services/history');
const antikeimService = require('~services/antikem');
const inFieldService = require('~services/in_field');
const etaireiaService = require('~services/etaireia');
const projectService = require('~services/project');
const authService = require('~services/authentication');

module.exports.get_history = async (req, res, next) => {
	try {
		const { username, table } = req.query;

		let history;

		if (username && table) {
			history = await historyService.getHistoryByTableAndUser(username, table);
		} else if (table) {
			history = await historyService.getHistoryByTable(table);
		} else if (username) {
			history = await historyService.getHistoryByUser(username);
		} else {
			history = await historyService.getHistory();
		}

		res.status(200).json(history);
	} catch (error) {
		next(error);
	}
};

module.exports.add_recruiter = async (req, res, next) => {
	try {
		const userInfo = req.body;
		const { username } = authService.decode(req.headers.authorization);

		// Check if username is free
		await userService.isUsernameFree(userInfo.username);

		// Add form data to user and recruiter tables and update history
		await userService.createUser(userInfo);
		await historyService.updateHistory(username);

		await recruiterService.createRecruiter(userInfo);
		await historyService.updateHistory(username);

		res.status(200).json(userInfo);
	} catch (error) {
		next(error);
	}
};

module.exports.add_candidate = async (req, res, next) => {
	try {
		let userInfo = req.body;
		const { username } = authService.decode(req.headers.authorization);

		// Check if username is free
		await userService.isUsernameFree(userInfo.username);

		// Add form data to user and candidate tables and update history
		await userService.createUser(userInfo);
		await historyService.updateHistory(username);

		await candidateService.createCandidate(userInfo);
		await historyService.updateHistory(username);

		// Check if there form has degree and language data and add
		// them to the database.
		if (userInfo.languages) {
			await languageService.addLanguagesToUser(
				userInfo.username,
				userInfo.languages
			);
		}

		if (userInfo.degree) {
			userInfo.idryma = await degreeService.getIdryma(userInfo.degree);

			await degreeService.addDegreeToUser(userInfo);
		}

		if (userInfo.projects) {
			for (let i = 0; i < userInfo.projects.length; i++) {
				const project = userInfo.projects[i];
				await projectService.createProject(userInfo.username, i + 1, project);
			}
		}

		res.status(200).json(userInfo);
	} catch (error) {
		next(error);
	}
};
module.exports.add_antikeim = async (req, res, next) => {
	try {
		const antikeim = req.body;

		// Validate antikeim title and belongs
		await antikeimService.antikeimExists(antikeim.title);
		if (antikeim.belongs) {
			await antikeimService.validateField(antikeim.belongs);
		}

		// Add antikeim to database
		await antikeimService.createAntikeim(antikeim);

		res.status(200).json(antikeim);
	} catch (error) {
		next(error);
	}
};
module.exports.add_in_field = async (req, res, next) => {
	try {
		const data = req.body;

		// Validate form data
		await Promise.all([
			inFieldService.isFirmInField(data.firmName, data.antikeim),
			etaireiaService.validateName(data.firmName),
			antikeimService.validateField(data.antikeim),
		]);

		// Add data to in_field table
		await inFieldService.addAntikeimToFirm(data);

		res.status(200).json(data);
	} catch (error) {
		next(error);
	}
};
