const router = require('express').Router();
const controller = require('./controller');

router.get('/history', controller.get_history);

router.post('/recruiter', controller.add_recruiter);

router.post('/candidate', controller.add_candidate);

router.post('/antikeim', controller.add_antikeim);

router.post('/field', controller.add_in_field);

module.exports = router;
