# Routes

## GET @ /get/users

returns

```Javascript
    [String]
```

## GET @ /get/history/tables

returns

```Javascript
    [
        "user",
        "etaireia",
        "recruiter",
        "candidate",
        "job"
    ]
```

## GET @ /get/firms

returns

```Javascript
    [...,
        {
            "AFM": String,
            "name": String
        },
    ]
```

## GET @ /get/languages

returns

```Javascript
    [
        "EN",
        "FR",
        "SP",
        "GR"
    ]
```

## GET @ /get/degrees

returns

```Javascript
    [String]
```

## GET @ /get/fields

returns

```Javascript
    [String]
```
