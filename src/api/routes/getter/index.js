const router = require('express').Router();
const controller = require('./controller');

router.get('/users', controller.get_users);

router.get('/history/tables', controller.get_tables);

router.get('/firms', controller.get_firms);

router.get('/languages', controller.get_languages);

router.get('/degrees', controller.get_degrees);

router.get('/fields', controller.get_fields);

module.exports = router;
