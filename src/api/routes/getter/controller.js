const userService = require('~services/user');
const etaireiaService = require('~services/etaireia');
const degreeService = require('~services/degree');
const antikeimService = require('~services/antikem');

module.exports.get_users = async (req, res, next) => {
	try {
		let response = [];
		const users = await userService.getAll();

		users.forEach(user => response.push(user.username));

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_tables = async (req, res, next) => {
	try {
		const response = ['user', 'etaireia', 'recruiter', 'candidate', 'job'];

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_firms = async (req, res, next) => {
	try {
		const response = await etaireiaService.getAll();

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_languages = async (req, res, next) => {
	try {
		const response = ['EN', 'FR', 'SP', 'GR'];

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_degrees = async (req, res, next) => {
	try {
		const degrees = await degreeService.getAll();

		let response = [];

		degrees.forEach(degree => response.push(degree.titlos));

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_fields = async (req, res, next) => {
	try {
		const fields = await antikeimService.getAll();

		let response = [];

		fields.forEach(field => response.push(field.title));

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};
