# Actions

## Get candidate profile;

```
GET @ /candidate/profile
```

## Get candidate applications

```
GET @ /candidate/applications/all
```

## Apply for job:

```
POST @ /candidate/apply/:jobId
```

## Update profile

```
PUT @ /candidate/profile
```

## Cancel application

```
DELETE @ /candidate/cancel/:jobId
```

# Routes

## GET @ /candidate/profile

returns

```Javascript
    {
        "username": String,
        "name": String,
        "surname": String,
        "reg_date": Date,
        "email": String,
        "bio": String,
        "sistatikes": String,
        "certificates": String
    }
```

## GET @ /candidate/applications/all

returns

```Javascript
    [
        ...,
        {
        "id": Number,
        "start_date": Date,
        "salary": Number,
        "position": String,
        "edra": String,
        "recruiter": String, // recruiter username
        "announce_date": Date,
        "submission_date": Date,
        "status": String,
        "message": String
    },
    ]
```

## POST @ /candidate/apply/:jobId

expects valid job id

## PUT @ /candidate/profile

expects

```Javascript
    {
        "name": String,
        "surname": String,
        "reg_date": Date,
        "email": String,
        "bio": String,
        "sistatikes": String,
        "certificates": String
    }
```

## DELETE @ /candidate/cancel/:jobId

expects valid job id
