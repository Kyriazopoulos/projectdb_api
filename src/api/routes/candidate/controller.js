const userService = require('~services/user');
const candidateService = require('~services/candidate');
const authService = require('~services/authentication');
const appService = require('~services/application');
const jobService = require('~services/job');
const interviewService = require('~services/interview');
const requireService = require('~services/requires');

const moment = require('moment');

module.exports.get_candidate = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		const [user, candidate] = await Promise.all([
			userService.getByUsername(username),
			candidateService.getByUsername(username),
		]);

		//Hide password property
		const response = {
			username: user.username,
			name: user.name,
			surname: user.surname,
			reg_date: user.reg_date,
			email: user.email,
			bio: candidate.bio,
			sistatikes: candidate.sistatikes,
			certificates: candidate.certificates,
		};

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

// Need to fix leaderboard stored procedure, route is fully functional
module.exports.get_applications = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		const applications = await appService.getUserApplications(username);

		let jobs = [];

		for (let i = 0; i < applications.length; i++) {
			const app = applications[i];
			const job = await jobService.getById(app.job_id);

			const now = moment();

			if (moment(now).isBefore(job.submission_date)) {
				job.status = 'Open';
				job.message = 'The job is still open for submission';
			} else {
				const [jobApplications, jobInterviews] = await Promise.all([
					appService.getJobApplications(job.id),
					interviewService.getJobInterviews(job.id),
				]);

				if (jobApplications.length > jobInterviews.length) {
					job.status = 'Assessing';
					job.message = 'The job is under assessment';
				} else {
					job.status = 'Closed';
					job.message = 'Your ranking will be announced';
				}
			}

			jobs.push(job);
		}

		res.status(200).json(jobs);
	} catch (error) {
		next(error);
	}
};

module.exports.get_jobs_not_applied = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		const jobs = await jobService.getAll();

		let response = [];

		for (let i = 0; i < jobs.length; i++) {
			const job = jobs[i];
			const application = await appService.getApplication(username, job.id);
			if (application.length === 0) {
				const requirements = await requireService.getByJob(job.id);
				let requires = [];

				requirements.forEach(r => requires.push(r.antikeim_title));
				response.push({ ...job, requires });
			}
		}

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.apply_for_job = async (req, res, next) => {
	try {
		const decoded = authService.decode(req.headers.authorization);
		await appService.createApplication(decoded.username, req.params.jobId);

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};

module.exports.update_candidate = async (req, res, next) => {
	try {
		const updateOpts = req.body;
		const { username } = authService.decode(req.headers.authorization);

		await userService.updateUser(username, updateOpts);
		await candidateService.updateCandidate(username, updateOpts);

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};

module.exports.cancel_application = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		await appService.cancelApplication(username, req.params.jobId);

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};
