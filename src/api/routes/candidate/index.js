const router = require('express').Router();
const controller = require('./controller');

router.get('/profile', controller.get_candidate);

router.get('/applications/all', controller.get_applications);

router.get('/notApplied', controller.get_jobs_not_applied);

router.post('/apply/:jobId', controller.apply_for_job);

router.put('/profile', controller.update_candidate);

router.delete('/cancel/:jobId', controller.cancel_application);

module.exports = router;
