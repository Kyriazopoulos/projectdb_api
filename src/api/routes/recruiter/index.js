const router = require('express').Router();
const controller = require('./controller');

router.get('/etaireia', controller.get_etaireia);

router.get('/profile', controller.get_profile);

router.get('/jobs', controller.get_recruiter_jobs);

router.get('/etaireia/jobs', controller.get_etaireia_jobs);

router.post('/job', controller.add_job);

router.post('/interview', controller.add_interview);

router.put('/etaireia', controller.update_etaireia);

router.put('/job/:jobId', controller.update_job);

router.put('/profile', controller.update_profile);

module.exports = router;
