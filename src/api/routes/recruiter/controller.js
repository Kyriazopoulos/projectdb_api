const etaireiaService = require('~services/etaireia');
const recruiterService = require('~services/recruiter');
const userService = require('~services/user');
const inFieldService = require('~services/in_field');
const authService = require('~services/authentication');
const jobService = require('~services/job');
const historyService = require('~services/history');
const requireService = require('~services/requires');
const appService = require('~services/application');
const interviewService = require('~services/interview');
const scoreService = require('~services/score');

const moment = require('moment');

module.exports.get_etaireia = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		const etaireia = await etaireiaService.getRecruiterEtaireia(username);
		const fieldTitles = await inFieldService.getFieldsByFirm(etaireia.AFM);

		let response = { etaireia, fields: [] };

		fieldTitles.forEach(f => response.fields.push(f.ftitle));

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_profile = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		const [user, recruiter] = await Promise.all([
			userService.getByUsername(username),
			recruiterService.getByUsername(username),
		]);

		const response = {
			username: user.username,
			name: user.name,
			surname: user.surname,
			reg_date: user.reg_date,
			email: user.email,
			exp_years: recruiter.exp_years,
			firm: recruiter.firm,
		};

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_recruiter_jobs = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		const jobs = await jobService.getByRecruiter(username);

		let response = [];

		for (let i = 0; i < jobs.length; i++) {
			const job = jobs[i];
			let requires = [];

			const requirements = await requireService.getByJob(job.id);

			requirements.forEach(r => requires.push(r.antikeim_title));

			let jobInfo = { ...job, requires };

			if (moment().isAfter(job.submission_date)) {
				jobInfo.status = 'Closed';
			} else {
				jobInfo.status = 'Open';
			}

			const applications = await appService.getJobApplications(job.id);
			jobInfo.application_number = applications.length;

			response.push(jobInfo);
		}

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.get_etaireia_jobs = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		const etaireia = await etaireiaService.getRecruiterEtaireia(username);
		const jobs = await jobService.getByEtaireia(etaireia.AFM);

		let response = [];

		for (let i = 0; i < jobs.length; i++) {
			const job = jobs[i];
			let requires = [];

			const requirements = await requireService.getByJob(job.id);

			requirements.forEach(r => requires.push(r.antikeim_title));

			response.push({ ...job, requires });
		}

		res.status(200).json(response);
	} catch (error) {
		next(error);
	}
};

module.exports.add_job = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);

		let jobInfo = req.body;
		jobInfo.announce_date = moment().format('YYYY-MM-DD');

		jobInfo.start_date = moment(req.body.start_date).format('YYYY-MM-DD');
		jobInfo.submission_date = moment(req.body.submission_date).format(
			'YYYY-MM-DD'
		);

		await jobService.createJob(username, jobInfo);
		await historyService.updateHistory(username);

		const recruiterJobs = await jobService.getByRecruiter(username);
		const lastIndex = recruiterJobs.length - 1;

		for (let i = 0; i < jobInfo.requires.length; i++) {
			const requirement = jobInfo.requires[i];
			await requireService.addRequires(
				recruiterJobs[lastIndex].id,
				requirement
			);
		}

		res.status(200).json(jobInfo);
	} catch (error) {
		next(error);
	}
};

module.exports.add_interview = async (req, res, next) => {
	try {
		const { jobId, candidate } = req.query;
		let interview = req.body;

		interview.date = moment(interview.date).format('YYYY-MM-DD HH:MM:SS');

		await interviewService.createInterview(jobId, candidate, interview);
		const newInterview = await interviewService.getById(candidate, jobId);

		await scoreService.createScore(newInterview.int_id, interview);

		res.status(200).json(interview);
	} catch (error) {
		next(error);
	}
};

module.exports.update_etaireia = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		const updateOpts = req.body;

		const etaireia = await etaireiaService.getRecruiterEtaireia(username);
		const afm = etaireia.AFM;

		await etaireiaService.updateEtaireia(afm, updateOpts.etaireia);
		await historyService.updateHistory(username);

		await inFieldService.deleteFirmFields(afm);

		const { fields } = updateOpts;
		for (let i = 0; i < fields.length; i++) {
			const field = fields[i];
			await inFieldService.addAntikeimToFirm({
				firmName: afm,
				antikeim: field,
			});
		}

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};

module.exports.update_job = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		const id = req.params.jobId;

		let jobInfo = req.body;
		jobInfo.announce_date = moment(jobInfo.announce_date).format('YYYY-MM-DD');

		jobInfo.start_date = moment(req.body.start_date).format('YYYY-MM-DD');
		jobInfo.submission_date = moment(req.body.submission_date).format(
			'YYYY-MM-DD'
		);

		await jobService.updateJob(id, jobInfo);
		await historyService.updateHistory(username);

		await requireService.deleteJobRequires(id);

		for (let i = 0; i < jobInfo.requires.length; i++) {
			const requirement = jobInfo.requires[i];
			await requireService.addRequires(id, requirement);
		}

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};

module.exports.update_profile = async (req, res, next) => {
	try {
		const { username } = authService.decode(req.headers.authorization);
		const updateOpts = req.body;

		await userService.updateUser(username, updateOpts);
		await historyService.updateHistory(username);

		await recruiterService.updateRecruiter(username, updateOpts);
		await historyService.updateHistory(username);

		res.sendStatus(200);
	} catch (error) {
		next(error);
	}
};
