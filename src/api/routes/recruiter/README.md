# Actions

## Get etaireia info:

```
GET @ /recruiter/etaireia
```

## Get recruiter profile:

```
GET @ /recruiter/profile
```

## Get jobs posted by recruiter:

```
GET @ /recruiter/jobs
```

## Get jobs posted by recruiter etaireia:

```
GET @ /recruiter/etaireia/jobs
```

## Create a job:

```
POST @ recruiter/job
```

## Create interview:

```
POST @ recruiter/interview?candidate={username}&jobId={jobId}
```

## Update etaireia:

```
PUT @ recruiter/etaireia
```

## Update job:

```
PUT @ /recruiter/job/:jobId
```

## Update recruiter profile:

```
PUT @ /recruiter/profile
```

# Routes

## GET @ /recruiter/etaireia

returns

```Javascript
    {
    "etaireia": {
        "AFM": String, // acts as id
        "DOY": String,
        "name": String,
        "tel": Number,
        "street": String,
        "num": Number,
        "country": String
    },
    "fields": [String] // field titles (id)
}
```

## GET @ /recruiter/profile

returns

```Javascript
    {
        "username": String, // Id
        "name": String,
        "surname": String,
        "reg_date": Date,
        "email": String,
        "exp_years": Number,
        "firm": String // Firm id
    }
```

## GET @ /recruiter/jobs

returns

```Javascript
    [
        {
        "id": Number, // Job id
        "start_date": Date,
        "salary": Number,
        "position": String,
        "edra": String,
        "recruiter": String, // recruiter username (id)
        "announce_date": Date,
        "submission_date": Date,
        "requires": [String], // Field titles (id)
        "status": String
    },
    ]
```

## GET @ /recruiter/etaireia/jobs

returns

```Javascript
    [
        {
        "id": Number, // Job id
        "start_date": Date,
        "salary": Number,
        "position": String,
        "edra": String,
        "recruiter": String, // recruiter username (id)
        "announce_date": Date,
        "submission_date": Date,
        "requires": [String], // Field titles (id)
    },
    ]
```

## POST @ recruiter/job

expects

```Javascript
    {
        "start_date": Date,
        "salary": Number,
        "position": String, // position name
        "edra": String,
        "submission_date": Date,
        "requires": [String] // field titles
    }
```

## POST @ recruiter/interview?candidate={username}&jobId={jobId}

expects

```Javascript
    {
    "date": Date,
    "duration": String, // format HH:MM:SS
    "comments": String,
    "personality": Number, // 0-5
    "education": Number, // 0-5
    "experience": Number // 0-5
}
```

returns

```Javascript
    {
    "date": Date,
    "duration": String, // format HH:MM:SS
    "comments": String,
    "personality": Number, // 0-5
    "education": Number, // 0-5
    "experience": Number // 0-5
}
```

## PUT @ recruiter/etaireia

expects

```Javascript
    {
    "etaireia": {
        "AFM": String, // acts as id
        "DOY": String,
        "name": String,
        "tel": Number,
        "street": String,
        "num": Number,
        "country": String
    },
    "fields": [String] // field titles (id)
}
```

## PUT @ /recruiter/job/:jobId

expects

```Javascript
    {
        "start_date": Date,
        "salary": Number,
        "position": String, // position name
        "edra": String,
        "submission_date": Date,
        "requires": [String] // field titles
    }
```

## PUT @ /recruiter/profile

expects

```Javascript
        {
        "username": String, // Id
        "name": String,
        "surname": String,
        "reg_date": Date,
        "email": String,
        "exp_years": Number,
        "firm": String // Firm id
    }
```
