const dbService = require('~services/database');

const addLanguagesToUser = async (username, languages) => {
	const query = `INSERT INTO languages VALUES ((SELECT username FROM candidate WHERE username='${username}'), '${languages}');`;

	await dbService.executeQuery(query);
};

module.exports = {
	addLanguagesToUser,
};
