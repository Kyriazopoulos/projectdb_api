const dbService = require('~services/database');

const getByUsername = async username => {
	const query = `SELECT exp_years, firm FROM recruiter WHERE username='${username}';`;

	const result = await dbService.executeQuery(query);

	return result[0];
};

const createRecruiter = async recruiter => {
	const query = `INSERT INTO recruiter VALUES ((SELECT username FROM user WHERE username='${recruiter.username}'), '${recruiter.experience}', (SELECT AFM FROM etaireia WHERE name='${recruiter.firm}'));`;

	await dbService.executeQuery(query);
};

const updateRecruiter = async (username, updateOpts) => {
	const query = `UPDATE recruiter SET exp_years='${updateOpts.exp_years}', firm='${updateOpts.firm}' WHERE username='${username}';`;

	await dbService.executeQuery(query);
};

module.exports = {
	getByUsername,
	createRecruiter,
	updateRecruiter,
};
