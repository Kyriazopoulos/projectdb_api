const mysql = require('mysql2');
const config = require('~root/config.json');

const executeQuery = query => {
	const connection = mysql.createConnection({
		host: config.database.host,
		user: config.database.user,
		password: config.database.password,
		database: config.database.name,
	});

	return new Promise((resolve, reject) => {
		connection.query(query, (err, rows, fields) => {
			connection.end();
			if (err) {
				reject(err);
			} else {
				resolve(JSON.parse(JSON.stringify(rows)));
			}
		});
	});
};

module.exports.executeQuery = executeQuery;
