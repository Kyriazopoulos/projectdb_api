const dbService = require('~services/database');

const createScore = async (interviewId, score) => {
	const query = `INSERT INTO score VALUES(${interviewId}, ${score.personality}, ${score.education}, ${score.experience});`;

	await dbService.executeQuery(query);
};

module.exports = { createScore };
