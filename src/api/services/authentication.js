const jwt = require('jsonwebtoken');
const config = require('~root/config.json');

/**
 * Create a jsonwebtoken from email and id
 * @param {String} username
 * @param {String} type
 */
const sign = (username, type) => {
	const token = jwt.sign(
		{
			username,
			type,
		},
		config.authentication.key,
		{ expiresIn: config.authentication.session }
	);

	return token;
};

/**
 * Verify a jsonwebtoken
 * @param {String} token
 */
const verify = token => {
	try {
		const decoded = jwt.verify(token, config.authentication.key);
		return decoded;
	} catch (error) {
		let err = new Error('Auth failed');
		err.status = 401;
		throw err;
	}
};

const decode = token => {
	const decoded = jwt.decode(token);
	return decoded;
};

module.exports = {
	sign,
	verify,
	decode,
};
