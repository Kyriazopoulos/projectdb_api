const dbService = require('~services/database');

const getUserApplications = async candUsername => {
	const query = `SELECT * FROM applies WHERE cand_usrname='${candUsername}';`;
	const applications = await dbService.executeQuery(query);
	return applications;
};

const getJobApplications = async jobId => {
	const query = `SELECT * FROM applies WHERE job_id='${jobId}';`;
	const applications = await dbService.executeQuery(query);
	return applications;
};

const getApplication = async (username, jobId) => {
	const query = `SELECT * FROM applies where cand_usrname='${username}' AND job_id='${jobId}';`;
	const result = await dbService.executeQuery(query);
	return result;
};

const createApplication = async (candUsername, jobId) => {
	const query = `INSERT INTO applies VALUES('${candUsername}','${jobId}');`;
	await dbService.executeQuery(query);
};

const cancelApplication = async (candUsername, jobId) => {
	const query = `DELETE FROM applies WHERE cand_usrname='${candUsername}' AND job_id='${jobId}';`;
	await dbService.executeQuery(query);
};

module.exports = {
	getUserApplications,
	getJobApplications,
	getApplication,
	createApplication,
	cancelApplication,
};
