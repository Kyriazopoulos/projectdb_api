const dbService = require('~services/database');

const getHistory = async () => {
	const query = `SELECT * FROM history;`;
	const result = dbService.executeQuery(query);
	return result;
};

const updateHistory = async username => {
	const query = `CALL update_history('${username}')`;
	await dbService.executeQuery(query);
};

const getHistoryByTableAndUser = async (username, table) => {
	const query = `SELECT * FROM history WHERE username='${username}' AND on_table='${table}';`;
	const result = await dbService.executeQuery(query);
	return result;
};

const getHistoryByUser = async username => {
	const query = `SELECT * FROM history WHERE username='${username}';`;
	const result = await dbService.executeQuery(query);
	return result;
};

const getHistoryByTable = async table => {
	const query = `SELECT * FROM history WHERE on_table='${table}';`;
	const result = await dbService.executeQuery(query);
	return result;
};

module.exports = {
	getHistory,
	updateHistory,
	getHistoryByTableAndUser,
	getHistoryByUser,
	getHistoryByTable,
};
