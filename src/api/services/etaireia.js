const dbService = require('~services/database');

const getAll = async () => {
	const query = 'SELECT AFM, name FROM etaireia';
	const result = await dbService.executeQuery(query);
	return result;
};

const validateName = async name => {
	const query = `SELECT * FROM etaireia WHERE name='${name}';`;

	const result = await dbService.executeQuery(query);

	if (result.length !== 1) throw new Error('Invalid firm name');
};

const getRecruiterEtaireia = async username => {
	const query = `select etaireia.* from etaireia inner join recruiter on recruiter.firm=etaireia.AFM where recruiter.username='${username}';`;

	const result = await dbService.executeQuery(query);

	return result[0];
};

const updateEtaireia = async (afm, updateOpts) => {
	const query = `UPDATE etaireia SET tel=${updateOpts.tel}, street='${updateOpts.street}', num=${updateOpts.num}, city='${updateOpts.city}', country='${updateOpts.country}' WHERE AFM='${afm}';`;

	await dbService.executeQuery(query);
};

module.exports = {
	getAll,
	validateName,
	getRecruiterEtaireia,
	updateEtaireia,
};
