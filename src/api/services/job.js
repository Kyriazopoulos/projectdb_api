const dbService = require('~services/database');

const getAll = async () => {
	const query = 'SELECT * FROM job;';
	const result = await dbService.executeQuery(query);
	return result;
};

const getById = async id => {
	const query = `SELECT * FROM job WHERE id='${id}';`;
	const job = await dbService.executeQuery(query);

	if (job.length !== 1) throw new Error('Invalid job id');

	return job[0];
};

const getByRecruiter = async username => {
	const query = `SELECT * FROM job WHERE recruiter='${username}';`;
	const jobs = await dbService.executeQuery(query);

	return jobs;
};

const getByEtaireia = async afm => {
	const query = `SELECT job.id, job.start_date, job.salary, job.position, job.edra, job.recruiter, job.announce_date, job.submission_date FROM job INNER JOIN recruiter ON recruiter.username=job.recruiter INNER JOIN etaireia ON etaireia.AFM=recruiter.firm WHERE etaireia.AFM='${afm}';`;

	const jobs = await dbService.executeQuery(query);

	return jobs;
};

const createJob = async (recruiterUsername, job) => {
	const query = `INSERT INTO job (start_date, salary, position, edra, recruiter, announce_date, submission_date) VALUES 
	('${job.start_date}', ${job.salary}, '${job.position}', '${job.edra}', '${recruiterUsername}', '${job.announce_date}', '${job.submission_date}');`;

	await dbService.executeQuery(query);
};

const updateJob = async (id, job) => {
	const query = `UPDATE job SET start_date='${job.start_date}', salary=${job.salary}, position='${job.position}', edra='${job.edra}' WHERE id=${id};`;

	await dbService.executeQuery(query);
};

module.exports = {
	getAll,
	getById,
	getByRecruiter,
	getByEtaireia,
	createJob,
	updateJob,
};
