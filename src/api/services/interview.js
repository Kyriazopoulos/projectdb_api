const dbService = require('~services/database');

const getJobInterviews = async jobId => {
	const query = `SELECT DISTINCT cand_usrn FROM interview WHERE job_id='${jobId}';`;
	const applications = await dbService.executeQuery(query);
	return applications;
};

const getById = async (candidate, job) => {
	const query = `SELECT * FROM interview WHERE job_id=${job} AND cand_usrn='${candidate}';`;
	const result = await dbService.executeQuery(query);

	if (result.length !== 1) throw new Error('Invalid interview');

	return result[0];
};

const createInterview = async (jobId, candidateUsername, interview) => {
	const query = `INSERT INTO interview(job_id, cand_usrn, on_date, duration, comments, pers_score) VALUES(${jobId}, '${candidateUsername}', '${interview.date}', '${interview.duration}', '${interview.comments}', ${interview.personality});`;

	await dbService.executeQuery(query);
};

module.exports = {
	getJobInterviews,
	getById,
	createInterview,
};
