const dbService = require('~services/database');

const getAll = async () => {
	const query = 'SELECT title FROM antikeim;';

	const result = await dbService.executeQuery(query);

	return result;
};

const antikeimExists = async title => {
	const query = `SELECT title FROM antikeim WHERE title='${title}';`;

	const result = await dbService.executeQuery(query);

	if (result.length !== 0) throw new Error('Antikeim exists');
};

const createAntikeim = async antikeim => {
	let query = `INSERT INTO antikeim(title, descr) VALUES('${antikeim.title}', '${antikeim.description}');`;
	if (antikeim.belongs) {
		query = `INSERT INTO antikeim VALUES('${antikeim.title}', '${antikeim.description}', '${antikeim.belongs}');`;
	}
	await dbService.executeQuery(query);
};

const validateField = async title => {
	const query = `SELECT * FROM antikeim WHERE title='${title}';`;

	const result = await dbService.executeQuery(query);

	if (result.length !== 1) throw new Error('Invalid antikeim');
};

module.exports = {
	getAll,
	antikeimExists,
	createAntikeim,
	validateField,
};
