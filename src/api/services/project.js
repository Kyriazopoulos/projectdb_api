const dbService = require('~services/database');

const getUserProjects = async username => {
	const query = `SELECT * FROM project WHERE candid='${username}';`;

	const projects = await dbService.executeQuery(query);
	return projects;
};

const createProject = async (username, number, project) => {
	const query = `INSERT INTO project VALUES('${username}',${number},'${project.description}', '${project.url}');`;

	await dbService.executeQuery(query);
};

module.exports = {
	getUserProjects,
	createProject,
};
