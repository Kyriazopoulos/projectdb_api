const dbService = require('~services/database');

const getAll = async () => {
	const query = 'SELECT titlos FROM degree;';
	const result = await dbService.executeQuery(query);
	return result;
};

const getIdryma = async degree => {
	const query = `SELECT idryma FROM degree WHERE titlos='${degree}';`;

	const result = await dbService.executeQuery(query);

	return result[0].idryma;
};

const addDegreeToUser = async user => {
	const query = `INSERT INTO has_degree VALUES ('${user.degree}', '${user.idryma}', '${user.username}', '${user.graduation}', '${user.grade}');`;

	await dbService.executeQuery(query);
};

module.exports = {
	getAll,
	getIdryma,
	addDegreeToUser,
};
