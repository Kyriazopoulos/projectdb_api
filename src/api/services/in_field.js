const dbService = require('~services/database');

const isFirmInField = async (name, antikeim) => {
	const query = `SELECT etaireia.name FROM etaireia INNER JOIN in_field ON
    etaireia.AFM=in_field.comp_afm WHERE etaireia.name='${name}' AND
    in_field.ftitle='${antikeim}'`;

	const result = await dbService.executeQuery(query);

	if (result.length !== 0) throw new Error('Firm already in field');
};

const addAntikeimToFirm = async data => {
	const query = `INSERT INTO in_field VALUES('${data.firmName}','${data.antikeim}');`;

	await dbService.executeQuery(query);
};

const getFieldsByFirm = async afm => {
	const query = `SELECT ftitle FROM in_field WHERE comp_afm='${afm}';`;

	const result = await dbService.executeQuery(query);

	return result;
};

const deleteFirmFields = async afm => {
	const query = `DELETE FROM in_field WHERE comp_afm=${afm};`;

	await dbService.executeQuery(query);
};

module.exports = {
	isFirmInField,
	addAntikeimToFirm,
	getFieldsByFirm,
	deleteFirmFields,
};
