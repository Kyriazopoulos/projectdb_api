const dbService = require('~services/database');
const historyService = require('~services/history');

const getAll = async () => {
	const query = 'SELECT username FROM user;';
	const users = await dbService.executeQuery(query);
	return users;
};

const getByUsername = async username => {
	const query = `SELECT * FROM user WHERE username='${username}'; `;
	const user = await dbService.executeQuery(query);

	if (user.length !== 1) throw new Error('Invalid username');
	return user[0];
};

const getTypeByUsername = async username => {
	if (username === 'admin') return 'admin';
	const query = `SELECT * FROM candidate WHERE username='${username}';`;
	const user = await dbService.executeQuery(query);
	if (user.length === 1) return 'candidate';
	return 'recruiter';
};

const isUsernameFree = async username => {
	const query = `SELECT * FROM user WHERE username='${username}';`;
	const user = await dbService.executeQuery(query);

	if (user.length !== 0) {
		throw new Error('Username taken');
	}
};

const createUser = async user => {
	const query = `INSERT INTO user VALUES ('${user.username}', '${user.password}', '${user.firstname}', '${user.lastname}', CURDATE(),'${user.email}');`;

	await dbService.executeQuery(query);
};

const updateUser = async (username, updateOpts) => {
	const query = `UPDATE user SET password='${updateOpts.password}',name='${updateOpts.name}',surname='${updateOpts.surname}', email='${updateOpts.email}' WHERE username='${username}';`;

	await dbService.executeQuery(query);
	await historyService.updateHistory(username);
};

module.exports = {
	getAll,
	getByUsername,
	getTypeByUsername,
	isUsernameFree,
	createUser,
	updateUser,
};
