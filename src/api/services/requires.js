const dbService = require('~services/database');

const getByJob = async jobId => {
	const query = `SELECT antikeim_title FROM requires WHERE job_id=${jobId};`;
	const result = dbService.executeQuery(query);

	return result;
};

const addRequires = async (jobId, fieldTitle) => {
	const query = `INSERT INTO requires VALUES(${jobId}, '${fieldTitle}');`;
	await dbService.executeQuery(query);
};

const deleteJobRequires = async jobId => {
	const query = `DELETE FROM requires WHERE job_id=${jobId};`;
	await dbService.executeQuery(query);
};

module.exports = {
	getByJob,
	addRequires,
	deleteJobRequires,
};
