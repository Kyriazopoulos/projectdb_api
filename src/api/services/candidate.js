const dbService = require('~services/database');
const historyService = require('~services/history');

const getByUsername = async username => {
	const query = `SELECT * FROM candidate WHERE username='${username}';`;
	const candidate = await dbService.executeQuery(query);

	if (candidate.length !== 1) throw new Error('Invalid username');

	return candidate[0];
};

const createCandidate = async candidate => {
	const query = `INSERT INTO candidate VALUES ((SELECT username FROM user WHERE username='${candidate.username}'), '${candidate.bio}', '${candidate.recomendation}', '${candidate.certificates}');`;

	await Promise.all([
		dbService.executeQuery(query),
		historyService.updateHistory('admin'),
	]);
};

const updateCandidate = async (username, updateOpts) => {
	const query = `UPDATE candidate SET bio='${updateOpts.bio}', certificates='${updateOpts.certificates}', sistatikes='${updateOpts.sistatikes}' WHERE username='${username}';`;

	await dbService.executeQuery(query);
	await historyService.updateHistory(username);
};

module.exports = {
	getByUsername,
	createCandidate,
	updateCandidate,
};
