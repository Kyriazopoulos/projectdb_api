const express = require('express');
const config = require('~root/config.json');
const cors = require('cors');

const app = express();

const userRouter = require('~routes/user');
const adminRouter = require('~routes/admin');
const candidateRouter = require('~routes/candidate');
const recruiterRouter = require('~routes/recruiter');
const getterRouter = require('~routes/getter');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/user', userRouter);
app.use('/admin', adminRouter);
app.use('/candidate', candidateRouter);
app.use('/recruiter', recruiterRouter);
app.use('/get', getterRouter);

// If request doesn't get handled by the routers above
// throw error
app.use((error, req, res, next) => {
	if (error) {
		next(error);
	} else {
		const error = new Error('Not found');
		error.status = 404;
		next(error);
	}
});

app.use((error, req, res, next) => {
	res.status(error.status || 500);
	console.log(error);
	res.json({
		error: {
			message: error.message,
		},
	});
});

// Port
const port = config.backend.port;
app.listen(port, () => console.log(`Listening on port ${port}...`));
