create TABLE interview(
    int_id int(6) auto_increment not null,
    job_id int(4) not null,
    cand_usrn varchar(12) not null,
    on_date datetime not null,
    duration time not null,
    comments mediumtext,
    pers_score int(1),
    primary key (int_id)
)engine=innoDB;

create table score(
    interv int(10) not null,
    personality int(1),
    education int(1),
    experience int(1),
    constraint scr foreign key (interv) references interview(int_id)
    on update cascade on delete cascade
)engine=innoDB;

create table in_field(
    comp_afm char(9),
    ftitle varchar(36),
    foreign key (comp_afm) references etaireia(AFM) on update cascade,
    foreign key (ftitle) references antikeim(title) on update cascade    
)engine=innoDB;

create table history(
    hid int(15) auto_increment primary key,
    username varchar(12),
    on_date datetime not null,
    success int(1),
    action enum('update', 'delete', 'insert') not null,
    on_table varchar(12)
)engine=innoDB;

insert into user values('admin', 'admin123', 'ad', 'adminopoulos', curdate(), 'adminakias@email.com');

insert into interview (job_id, cand_usrn, on_date, duration, pers_score) values
(1, 'abrown', '2018-12-23 18:00:00', '01:00:00', 4),
(9, 'abrown', '2019-04-15 11:00:00', '00:15:00', 2),
(1, 'abrown', '2019-01-03 11:00:00', '00:20:00', 4),
(2, 'cleogeo', '2019-01-15 10:00:00', '00:15:00', 5),
(7, 'cleogeo', '2019-04-15 11:00:00', '00:15:00', 3),
(4, 'liagourma', '2018-12-20 13:00:00', '00:22:00', 2),
(10, 'liagourma', '2019-03-15 11:30:00', '00:15:00', 5),
(10, 'liagourma', '2019-03-27 10:00:00', '00:20:00', 5),
(2, 'lionarF', '2019-01-22 16:00:00', '00:10:00', 0),
(5, 'lionarF', '2019-02-13 10:00:00', '00:20:00', 3),
(7, 'lionarF', '2019-04-18 14:00:00', '00:21:00', 1),
(12, 'lionarF', '2019-01-17 15:00:00', '00:09:00', 2),
(2, 'zazahir23', '2019-01-18 10:20:00', '00:26:00', 4),
(3, 'zazahir23', '2018-09-15 12:40:00', '00:16:00', 3),
(8, 'zazahir23', '2019-04-29 15:40:00', '00:28:00', 5)
;

insert into score () values
(1, 4, 5, 5),
(2, 2, 4, 4),
(3, 4, 5, 5),
(4, 5, 3, 5),
(5, 3, 2, 5),
(6, 2, 2, 0),
(7, 5, 5, 5),
(8, 5, 4, 3),
(9, 0, 2, 2),
(10, 3, 2, 3),
(11, 1, 3, 3),
(12, 2, 2, 2),
(13, 4, 5, 5),
(14, 3, 2, 1),
(15, 5, 5, 5)
; 

 insert into antikeim (title, descr, belongs_to) values
('Data Analysis', 'Level one element, child of Computer Science', 'Computer Science'); 

insert into in_field () values
((select AFM from etaireia where AFM=023451232), 'Data Analysis'),
((select AFM from etaireia where AFM=023451232), 'Web Programming'),
((select AFM from etaireia where AFM=023451232), 'Mobile Apps'),
((select AFM from etaireia where AFM=18765549), 'NLP'),
((select AFM from etaireia where AFM=18765549), 'Graphics'),
((select AFM from etaireia where AFM=023451232), 'Animation'),
((select AFM from etaireia where AFM=123432211), 'Web Programming'),
((select AFM from etaireia where AFM=123432211), 'Mobile Apps'),
((select AFM from etaireia where AFM=123432211), 'Graphics'),
((select AFM from etaireia where AFM=123432211), 'Databases'),
((select AFM from etaireia where AFM=23122345), 'AI'),
((select AFM from etaireia where AFM=23122345), 'Algorithms'),
((select AFM from etaireia where AFM=561234561), 'Web Programming'),
((select AFM from etaireia where AFM=561234561), 'Animation')
; 

-- Erotima 3a

 select job.id as Job,  job.salary, user.name as Recruiter_name, user.surname Recruiter_surname,
etaireia.name as Firm, count(applies.cand_usrname) as Applied
from job inner join user on user.username=job.recruiter inner join recruiter
on recruiter.username=user.username inner join etaireia on
recruiter.firm=etaireia.AFM inner join applies on applies.job_id=job.id
where job.salary>1900
group by job.id; 

-- Erotima 3b

select candidate.username, candidate.certificates,
count(has_degree.cand_usrname) as degrees,
if (count(has_degree.cand_usrname)>1, avg(has_degree.grade), "N/A") as average_grade
from candidate inner join has_degree
on candidate.username=has_degree.cand_usrname
group by candidate.username; 

-- Erotima 3c

select user.username, count(applies.cand_usrname) as applied,
if (avg(job.salary)>1800, avg(job.salary), 'N/A') as average_salary
from user left join applies on user.username=applies.cand_usrname
inner join job on applies.job_id=job.id
group by user.username; 

-- Erotima 3d

select etaireia.name as Firm_name, requires.antikeim_title as field_title,
job.position as job_description
from etaireia inner join recruiter on
recruiter.firm=etaireia.AFM inner join job on
job.recruiter=recruiter.username inner join requires on
job.id=requires.job_id inner join antikeim on
requires.antikeim_title=antikeim.title
where etaireia.DOY like '_ Patras' and antikeim.title like '%Program%'; 

-- Erotima 3e

select recruiter.username as recruiter, count(job.id) as jobs_announced,
count(interview.job_id) as interviews,
if (count(job.id)>1, avg(job.salary), 0) as average_salary
from job inner join recruiter on
job.recruiter=recruiter.username left join interview on
interview.job_id=job.id
where job.id=interview.job_id and recruiter.username=job.recruiter
group by recruiter.username
order by average_salary desc;

-- Erotima 4a

delimiter $
create procedure leaderboard(in id int(4))
    begin
        declare numOfinterviews int;
        declare numOfapplications int;
        declare failedApplicants int;
        declare pers varchar(20);
        declare edu varchar(20);
        declare exper varchar(20);

        set pers='failed the interview ';
        set edu='inadequate education ';
        set exper='no prior experience ';

        select count(distinct cand_usrn)
        into numOfinterviews
        from interview
        where job_id=id;

        select count(distinct cand_usrname)
        into numOfapplications
        from applies
        where job_id=id;

        if(numOfapplications=numOfinterviews) then

            select interview.cand_usrn as candidate,
            count(interview.cand_usrn) as interviews,
            if (count(interview.cand_usrn)>1, avg(score.personality), score.personality) as personality,
            score.education as education, score.experience as experience,
            (personality+education+experience) as total
            from interview inner join score on interview.int_id=score.interv 
            where interview.job_id=id and not (personality=0 or experience=0 or education=0)
            group by candidate 
            order by total desc;

            select count(interview.int_id)
            into failedApplicants
            from interview
            inner join score on interview.int_id=score.interv
            where score.personality=0 or score.education=0
            or score.experience=0;

            if(failedApplicants>0) then

                select interview.cand_usrn as candidate,
                if (score.personality=0 and score.education=0 and score.experience=0, concat(pers,edu,exper),
                if (score.personality=0 and score.education=0, concat(pers,edu),
                if (score.personality=0 and score.experience=0, concat(pers,edu),
                if (score.experience=0 and score.education=0, concat(exper,edu),
                if (score.personality=0, pers,
                if (score.education=0, edu, exper)))))) as failed_requirement
                from interview inner join score on 
                interview.int_id=score.interv
                inner join job on job.id=interview.job_id
                where job.id=id and (score.personality=0 or score.education=0 or score.experience=0)
                group by interview.cand_usrn;
            end if;

        else
            select 'H aksiologhsh exei oloklhrothei';
        end if;
    end$
delimiter ;

delimiter $
create trigger validateDeletedApplication
before delete on applies
for each row
begin
    declare today date;
    declare submission date;

    set today=curdate();

    select job.submission_date 
    into submission from
    job inner join applies on 
    job.id=applies.job_id
    where job.id=old.job_id limit 1;  

    if (today>submission) then
        SIGNAL SQLSTATE VALUE '45000'
        SET MESSAGE_TEXT = 'Error: Submission date has passed!';
    end if;

end$
delimiter ;

delimiter $
create trigger ai_candidate
after insert on candidate
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'insert', 'candidate');
end$
delimiter ;

delimiter $
create trigger au_candidate
after update on candidate
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'update', 'candidate');
end$
delimiter ;

delimiter $
create trigger ad_candidate
after delete on candidate
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'delete', 'candidate');
end$
delimiter ;

delimiter $
create trigger ai_recruiter
after insert on recruiter
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'insert', 'recruiter');
end$
delimiter ;

delimiter $
create trigger au_recruiter
after update on recruiter
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'update', 'recruiter');
end$
delimiter ;

delimiter $
create trigger ad_recruiter
after delete on recruiter
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'delete', 'recruiter');
end$
delimiter ;

delimiter $
create trigger ai_user
after insert on user
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'insert', 'user');
end$
delimiter ;

delimiter $
create trigger au_user
after update on user
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'update', 'user');
end$
delimiter ;

delimiter $
create trigger ad_user
after delete on user
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'delete', 'user');
end$
delimiter ;

delimiter $
create trigger ai_job
after insert on job
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'insert', 'job');
end$
delimiter ;

delimiter $
create trigger au_job
after update on job
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'update', 'job');
end$
delimiter ;

delimiter $
create trigger ad_job
after delete on job
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'delete', 'job');
end$
delimiter ;

delimiter $
create trigger ai_etaireia
after insert on etaireia
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'insert', 'etaireia');
end$
delimiter ;

delimiter $
create trigger bu_etaireia
before update on etaireia
for each row
begin
    set new.AFM=old.AFM;
    set new.DOY=old.DOY;
    set new.name=old.name;

    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'update', 'etaireia');
end$
delimiter ;

delimiter $
create trigger ad_etaireia
after delete on etaireia
for each row
begin
    insert into history (on_date, success, action, on_table)
    values (now(), 1, 'delete', 'etaireia');
end$
delimiter ;

delimiter $ 
create procedure update_history(in usrn varchar(12))
    begin

        declare last_row int;
        select max(hid) into last_row
        from history;

        update history set username=usrn where hid=last_row;

    end$
delimiter ;